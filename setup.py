# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
from setuptools import setup, find_packages

import versioneer


INSTALL_REQUIREMENTS = {
    'setup': [
    ],
    'install': [
        'numpy==1.22.0',
        'matplotlib==3.5.1',
        'poliastro==0.16.3',
        'tqdm==4.64.0',
        'astropy==5.0.4',
    ],
}

if __name__ == '__main__':
    setup(
        name='cosmic_simulations',
        version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass(),
        description='Simple model of a Sol System',
        zip_safe=False,
        author='Rafał Kaczmarek, Przemysław Ziółkowski, Robert Janowski',
        author_email='rjanosqi@gmail.com, kaczmarek.rr@gmail.com',
        url='https://gitlab.com/zjadaczeramu/cosmic-symilations/',
        license='Other/Proprietary License',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Natural Language :: English',
            'Topic :: Scientific/Engineering',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.9',
        ],
        keywords='gravity simulation solar system spaceship trajectory',
        packages=find_packages(include=['cosmic_simulations']),
        include_package_data=True,
        setup_requires=INSTALL_REQUIREMENTS['setup'],
        install_requires=INSTALL_REQUIREMENTS['install'],
        extras_require=INSTALL_REQUIREMENTS,
        entry_points={
            'console_scripts': [],
        },
        ext_modules=[],
    )
