import cosmic_simulations
from cosmic_simulations import physics
import numpy as np

def test_acc():
    a=physics.getAcc(np.array([[1,0,1],[-1,0,-1]]),[10000,1000],1,0.01)
    result=-88.38669039269469
    assert a[0][0]==result
