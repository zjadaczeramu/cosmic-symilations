"""Basic tests to test testing system."""
import cosmic_simulations


def test_import():
    """Package testing stub."""
    assert cosmic_simulations.TEST_VAR == 1
    
def test_function():
    assert cosmic_simulations.f1() == 1
