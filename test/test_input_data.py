import cosmic_simulations
from cosmic_simulations import input_data

def test_mass():
    assert input_data.mass[0]==1.99e30

def test_pos():
    assert input_data.cords[4][0]==-4.8883e+10

def test_vel():
    assert input_data.velocities[5][2]==-4.8696e+03
