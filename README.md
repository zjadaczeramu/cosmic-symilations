# Cosmic simulations

This project is developed as part of team programming course on Univerity of Warsaw Faculty of Physics. It's a functioning model of Solar System with Newtonian physics and `matplotlib` visualisation. It also features a possibility to find optimized transfer trajectories between planets based on Dario Izzo's algorithm for Lambert's problem.

## Installation

Using `pip`:

    pip install git+https://gitlab.com/zjadaczeramu/cosmic-symilations.git

Or you can just clone this repository:

    git clone https://gitlab.com/zjadaczeramu/cosmic-symilations.git

Then, to set environment and all dependencies, in the catalogue where this Readme is placed type:

    ./ci/test.sh

## Usage

If you've installed it via `pip` then start `python` and type:

    from cosmic_simulations import main

If you've cloned git repository then, in the main catalogue, type:

    python cosmic_simulations/main.py

You'll be asked to configure your simulation. Follow the displayed instructions or just click `Enter` twice and look at the stars.

## Known issues

Trajectories might be incorrect for high velocities. If too short time of flight is set, spaceship may not reach its destination.  <b>
Due to the closeness of the Sun trajectories that start at Mercury are also incorrect. It is now not possible to find them.

## Other

Project won't be developed further. Feel free to use it however you see fit.
