import numpy as np
from dataclasses import dataclass


@dataclass(init=True, repr=True, eq=True)
class Planets_data:
    M: float = 0
    Px: float = 0
    Py: float = 0
    Pz: float = 0
    Vx: float = 0
    Vy: float = 0
    Vz: float = 0

def return_data(names, mass, cords, velocities):
    return names, mass, cords, velocities


# data via https://www.mathworks.com/help/physmod/sm/ug/model-planet-orbit-due-to-gravity.html


Sun = Planets_data(1.99e30, 5.5850e+08, 5.5850e+08, 5.5850e+08, -1.4663, 11.1238, 4.8370
                        )
Mercury = Planets_data(3.30e23, 5.1979e+10, 7.6928e+09, -1.2845e+09, -1.5205e+04, 4.4189e+04, 2.5180e+04
                       )
Venus = Planets_data(4.87e24, -1.5041e+10, 9.7080e+10, 4.4635e+10, -3.4770e+04, -5.5933e+03, -316.8994
                     )
Earth = Planets_data(5.97e24, -1.1506e+09, -1.3910e+11, -6.0330e+10, 2.9288e+04, -398.5759, -172.5873
                     )
Mars = Planets_data(6.42e23, -4.8883e+10, -1.9686e+11, -8.8994e+10, 2.4533e+04, -2.7622e+03, -1.9295e+03
                    )
Jupiter = Planets_data(1.90e27, -8.1142e+11, 4.5462e+10, 3.9229e+10, -1.0724e+03, -1.1422e+04, -4.8696e+03
                       )
Saturn = Planets_data(1.90e27, -4.2780e+11, -1.3353e+12, -5.3311e+11, 8.7288e+03, -2.4369e+03, -1.3824e+03
                      )
Uranus = Planets_data(8.68e25, 2.7878e+12, 9.9509e+11, 3.9639e+08, -2.4913e+03, 5.5197e+03, 2.4527e+03
                      )
Neptun = Planets_data(1.02e26, 4.2097e+12, -1.3834e+12, -6.7105e+11, 1.8271e+03, 4.7731e+03, 1.9082e+03
                      )

names = ["Sun", "Mercury", "Venus", "Earth",
         "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]

mass = np.array([Sun.M, Mercury.M, Venus.M, Earth.M, Mars.M, Jupiter.M, Saturn.M, Uranus.M, Neptun.M])
cords = np.array([[Sun.Px, Sun.Py, Sun.Pz], [Mercury.Px, Mercury.Py, Mercury.Pz], [Venus.Px, Venus.Py, Venus.Pz],
                  [Earth.Px, Earth.Py, Earth.Pz], [Mars.Px, Mars.Py, Mars.Pz], [Jupiter.Px, Jupiter.Py, Jupiter.Pz],
                  [Saturn.Px, Saturn.Py, Saturn.Pz], [Uranus.Px, Uranus.Py, Uranus.Pz], [Neptun.Px, Neptun.Py, Neptun.Pz]])

velocities = np.array([[Sun.Vx, Sun.Vy, Sun.Vz], [Mercury.Vx, Mercury.Vy, Mercury.Vz], [Venus.Vx, Venus.Vy, Venus.Vz],
                  [Earth.Vx, Earth.Vy, Earth.Vz], [Mars.Vx, Mars.Vy, Mars.Vz], [Jupiter.Vx, Jupiter.Vy, Jupiter.Vz],
                  [Saturn.Vx, Saturn.Vy, Saturn.Vz], [Uranus.Vx, Uranus.Vy, Uranus.Vz], [Neptun.Vx, Neptun.Vy, Neptun.Vz]])
