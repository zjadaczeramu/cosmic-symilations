# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation

# import astronomical_object as ao
# import input_data as id

from cosmic_simulations import astronomical_object as ao
from cosmic_simulations import input_data as id
from cosmic_simulations import trajectory as tr
from cosmic_simulations import user_input as ui

spaceship,time_of_sim,start,destination,ts,te,plt_lim=ui.user_input()

# elev’ stores the elevation angle in the z plane.
# azim’ stores the azimuth angle in the x,y plane.
elev, azim = 20, 35
#plt_lim = 3e11 # 2 astronomical units 
dt = 86_164
colours = ['gold', 'slategray', 'bisque', 'cornflowerblue',
           'firebrick', 'peru', 'khaki', 'teal', 'royalblue']
sizes = [22, 2, 6, 6, 3, 10, 10, 10, 10]

names, mass, pos, vel0 = id.return_data(
    id.names, id.mass, id.cords, id.velocities)

fig = plt.figure("Solar system")
ax = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(ax)

ax.view_init(elev, azim)
ax.set_xlim(-plt_lim, plt_lim)
ax.set_ylim(-plt_lim, plt_lim)
ax.set_zlim(-plt_lim, plt_lim)

# ax.grid(False)
ax.format_coord = lambda x, y: ''  # hide coordinates on plot
fig.set_facecolor('black')  # black background
ax.set_facecolor('black')
ax.w_xaxis.set_pane_color((0.0, 0.0, 0.0, 0.0))
ax.w_yaxis.set_pane_color((0.0, 0.0, 0.0, 0.0))
ax.w_zaxis.set_pane_color((0.0, 0.0, 0.0, 0.0))

# zrobmy pudelko o grubosci x samej plaskiej zamiast scianki
num_of_stars = 1000
multi = 1.5
x, y, z = np.random.uniform(-plt_lim * multi,
                            plt_lim * multi, (3, num_of_stars))
zeros = np.linspace(-plt_lim, -plt_lim * multi, num_of_stars)
ax.scatter(zeros, y, z, s=0.5, c='w', marker="*")
ax.scatter(x, zeros, z, s=0.5, c='w', marker="*")
ax.scatter(x, y, zeros, s=0.5, c='w', marker="*")

# Creation of each planet in our system
# w inicjalizacji dostaje dodatkowo konkretny plot, na ktrym będzie wyświetlane.
planets = []
for i in range(len(pos)):
    planets.append(ao.Astronomical_Object(
        pos[i], vel0[i], mass[i], fig, ax, names[i], colours[i], sizes[i]))


radius = [(cor[0]**2 + cor[1]**2 + cor[2]**2)**0.5 for cor in pos]
total_v = [(cor[0]**2 + cor[1]**2 + cor[2]**2)**0.5 for cor in vel0]
periods = [(2 * np.pi * radius[i] / total_v[i]) for i in range(len(radius))]
trail_length = [int(period / dt /10) for period in periods]
trail_length[0] = 1

trails = []
for i in range(len(pos)):
    trails.append(ao.Trace(pos[i], trail_length[i], fig, ax))
        

planet_system = ao.Astronomical_Object(pos, vel0, mass, fig, ax)

def init():
    return []

if spaceship:
    _,vd,_,t0,tk,sind,_ = tr.trajectory(start,destination,
            ts,te,fig,ax)

def update(t):

    if spaceship and update.counter==t0:
        planets.append(ao.Astronomical_Object(
           planets[sind].pos,vd,10,fig,ax,
           'Spaceship','silver',5,'x'))
        trails.append(ao.Trace(planets[sind].pos,20,fig,ax))
        trail_length.append(20)
        planet_system.add(planets[-1].pos,planets[-1].vel,10)
    elif spaceship and update.counter==tk:
        planets.pop(-1)
        trails.pop(-1)
        trail_length.pop(-1)
        planet_system.remove_last()

    update.counter += 1
    new_pos = ao.Astronomical_Object.update_pos(
        planet_system)

    for step in range(len(planets)):
        distance = new_pos[step][0]
        trails[step].update_trace(new_pos[step])

        if any((wsp > plt_lim * 3 or wsp < -plt_lim * 3) for wsp in new_pos[step]):
            pass
        else:
            planets[step].update_img(new_pos[step])
            trails[step].update_image(trail_length[step])

        planets[step].points.set_zorder(distance)
        trails[step].points.set_zorder(distance - 1)


    trails_art = [trail.points for trail in trails]
    planets_art = [planet.points for planet in planets]

    artists = planets_art + trails_art
    return artists


update.counter = 0
nr_of_timesteps = time_of_sim
ani = FuncAnimation(fig, update, frames=nr_of_timesteps,
                    init_func=init, interval=30, blit=True, repeat=False)
plt.show()
# print("Nr. of iterations = ", update.counter)
