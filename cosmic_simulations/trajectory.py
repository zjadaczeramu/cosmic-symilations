# -*- coding: utf-8 -*-
import copy
import numpy as np
from poliastro.core import iod
from poliastro.iod import izzo
from astropy import units as u
from tqdm import tqdm

from cosmic_simulations import input_data as id
from cosmic_simulations import astronomical_object as ao

R=[695700,2440,6052,6371,3390,69911,58232,25362,24622]

def trajectory(start,destination,t0,tk,fig,ax):

    names, mass, coords, velstart = id.return_data(id.names,id.mass,id.cords,id.velocities)
    planet_system = ao.Astronomical_Object(
            coords, velstart, mass,fig,ax)
    k=ao.Astronomical_Object.G*planet_system.mass[0]*1e-9*u.km**3/u.s**2
    day=86_164*u.s/u.d
    delta=10e10*u.km/u.s
    sind=names.index(start)
    eind=names.index(destination)
    vd=[0,0,0]*u.km/u.s
    va=[0,0,0]*u.km/u.s
    old_percent=0
    vesc=np.sqrt(2*ao.Astronomical_Object.G*1e-9*planet_system.mass[sind]/R[sind])*u.km/u.s
    pbar=tqdm(total=100)
    while t0<tk:
        percent=100*t0/tk
        if int(percent)>old_percent:
            old_percent=int(percent)
            pbar.update(1)
        pos0=np.copy(planet_system.pos)
        vel0=np.copy(planet_system.vel)
        r1=list((planet_system.pos[sind]-planet_system.pos[0])/1000)*u.km
        v1=list((planet_system.vel[sind]-planet_system.pos[0])/1000)*u.km/u.s
        tkt=t0
        for j in range(t0+1,tk+1):
            planet_system.update_pos()
            tkt+=1
            r2=list((planet_system.pos[eind]-planet_system.pos[0])/1000)*u.km
            v2=list((planet_system.vel[eind]-planet_system.vel[0])/1000)*u.km/u.s
            tof=(tkt-t0)*u.d*day
            (new_vd,new_va),=izzo.lambert(k,r1,r2,tof)
            dvd=np.linalg.norm(new_vd-v1)
            dvd+=vesc
            dva=np.linalg.norm(new_va-v2)
            new_delta=dvd+dva
            if new_delta<delta:
                delta=new_delta
                vd=new_vd
                va=new_va
                tkf=tkt
                t0f=t0
                vk=np.copy(planet_system.vel)
        planet_system.reset(pos0,vel0)
        planet_system.update_pos()
        t0+=1
    planet_system.reset(coords,velstart)
    vd+=vel0[0]/1000*u.km/u.s
    va+=vk[0]/1000*u.km/u.s
    pbar.close()
    return delta.value*1000,vd.value*1000,va.value*1000,t0f,tkf,sind,eind
