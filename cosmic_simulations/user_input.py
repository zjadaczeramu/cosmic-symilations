def user_input():
    #default
    planet_list=['Mercury','Venus','Earth','Mars','Jupiter','Saturn','Uranus','Neptune','mercury','venus','earth','mars','jupiter','saturn','uranus','neptune','']
    outer=['Saturn','Uranus','Neptune']
    num0=['0','1','2','3','4','5','6','7']
    num1=['0','1','2','3','4','5','6']
    spaceship=False
    time_of_sim=365
    start='Earth'
    destination='Mars'
    time_of_start=0
    time_of_end=365
    plt_lim=3e11

    input0='k'
    while input0 not in ['y','Y','n','N','']:
        input0=input('Do you want to set a course for spaceship? (y/N, default N): ')

    if input0 in ['y','Y']:
        spaceship=True

        input1='k'
        print('Select where your mission starts:')
        print('Mercury (currently unsupported)')
        print('0) Venus')
        print('1) Earth')
        print('2) Mars')
        print('3) Jupiter')
        print('4) Saturn')
        print('5) Uranus')
        print('6) Neptune')
        while input1 not in planet_list and input1 not in num1:
            input1=input('(Default: Earth): ')
        if input1 in ['Venus','venus','0']:
            start='Venus'
        elif input1 in ['Earth','earth','1','']:
            start='Earth'
        elif input1 in ['Mars','mars','2']:
            start='Mars'
        elif input1 in ['Jupiter','jupiter','3']:
            start='Jupiter'
        elif input1 in ['Saturn','saturn','4']:
            start='Saturn'
        elif input1 in ['Uranus','uranus','5']:
            start='Uranus'
        elif input1 in ['Neptune','neptune','6']:
            start='Neptune'

        input2='k'
        print('Select where your mission ends:')
        print('0) Mercury')
        print('1) Venus')
        print('2) Earth')
        print('3) Mars')
        print('4) Jupiter')
        print('5) Saturn')
        print('6) Uranus')
        print('7) Neptune')
        while input2 not in planet_list and input2 not in num0:
            input2=input('(Default: Mars): ')
        if input2 in ['Mercury','mercury','0']:
            destination='Mercury'
        elif input2 in ['Venus','venus','1']:
            destination='Venus'
        elif input2 in ['Earth','earth','2']:
            destination='Earth'
        elif input2 in ['Mars','mars','3','']:
            destination='Mars'
        elif input2 in ['Jupiter','jupiter','4']:
            destination='Jupiter'
        elif input2 in ['Saturn','saturn','5']:
            destination='Saturn'
        elif input2 in ['Uranus','uranus','6']:
            destination='Uranus'
        elif input2 in ['Neptune','neptune','7']:
            destination='Neptune'

        input3='k'
        print('Choose the soonest possible start of mission (in days after the start of simulation).')
        print('Warning: trajectories may not be correctly set for very high velocities. Choosing too small time of flight may result in spaceship not reaching its destination.')
        while not input3.isdigit() and not input3=='':
            input3=input('(Default: 0): ')
            if input3=='':
                time_of_start=0
            elif input3.isdigit():
                time_of_start=int(input3)

        input4='k'
        print('Choose when the latest possible end of mission (in days after the start of simulation).')
        print('Warning: trajectories may not be correctly set for very high velocities. Choosing too small time of flight may result in spaceship not reaching its destination.')
        while not input4.isdigit() and not input4=='':
            input4=input('(Default: 365): ')
            if input4=='':
                time_of_end=365
            elif input4.isdigit():
                time_of_end=int(input4)

    input5='k'
    print('Choose the time of simulation (in days).')
    while not input5.isdigit() and not input5=='':
        input5=input('(Default: 365): ')
        if input5=='':
            time_of_sim=365
        elif input5.isdigit():
            time_of_sim=int(input5)

    if start in outer or destination in outer:
        print('One of chosen planets is beyond the default limits of the plot.')
        input6='k'
        while input6 not in ['y','Y','n','N','']:
            input6=input('Do you wish to expand it? (y/N, default y): ')
            if input6 in ['y','Y','']:
                plt_lim=3e12

    return spaceship,time_of_sim,start,destination,time_of_start,time_of_end,plt_lim

