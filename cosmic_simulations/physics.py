# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

def getAcc( pos, mass, G, softening ):
	x = pos[:,0:1]
	y = pos[:,1:2]
	z = pos[:,2:3]

	# matrix of distances between objects
	dx = x.T - x
	dy = y.T - y
	dz = z.T - z

	# matrix that stores 1/r^3 for all particle pairwise particle separations 
	inv_r3 = (dx**2 + dy**2 + dz**2 + softening**2)
	inv_r3[inv_r3>0] = inv_r3[inv_r3>0]**(-1.5)

	ax = G * (dx * inv_r3) @ mass
	ay = G * (dy * inv_r3) @ mass
	az = G * (dz * inv_r3) @ mass
	
	ax = ax.reshape(1,len(mass))
	ay = ay.reshape(1,len(mass))
	az = az.reshape(1,len(mass))
	# pack together the acceleration components
	a = np.vstack((ax,ay,az)).T

	return a

def r_lenght(poss):
    r = np.hypot(poss[:,0],poss[:,1])
    r = np.hypot(r,poss[:,2])
    r = np.where(r==0,10000,r)
    return r.astype('float64')

def v0(mass,r,G=1):  #przyjmując, że mass[0] = masa gwiazdy

    v = np.sqrt((G*mass[0][0])/r)
    
    return v.astype('float64')
