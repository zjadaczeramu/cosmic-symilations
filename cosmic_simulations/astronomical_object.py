# -*- coding: utf-8 -*-
import numpy as np
from cosmic_simulations import physics as ph

class Astronomical_Object:
    softening = 0.00001

    dt = 86_164  # (day = 23 h 56 min 4,091 s = 86164,09 s)
    G = 6.67e-11

    def __init__(self, pos, vel, mass,fig,ax, name="Planetary system", colour='green', size=1, icon='o'):
        self.pos = np.array(pos)
        self.vel = np.array(vel)
        self.mass = np.array(mass)
        self.name = name
        self.icon = icon  # symbol i kolor obiektu
        self.colour = colour
        self.size = size  # siz obiektu

        self.points, = ax.plot([], [], [], marker=icon, color=colour,
                               markersize=size, animated=True)

    def update_pos(self):
        acc = ph.getAcc(self.pos, self.mass, self.G, self.softening)
        self.vel += acc * self.dt / 2.0
        self.pos += self.vel * self.dt/2.0

        acc = ph.getAcc(self.pos, self.mass, self.G, self.softening)
        self.vel += acc * self.dt / 2.0
        self.pos += self.vel * self.dt/2.0

        return self.pos

    def update_img(self, new_pos):

        self.points.set_data_3d(
            new_pos[0], new_pos[1], new_pos[2])

    def add(self,pos,vel,mass):
        self.pos=np.append(self.pos,[pos],axis=0)
        self.vel=np.append(self.vel,[vel],axis=0)
        self.mass=np.append(self.mass,mass)
   
    def remove_last(self):
        self.pos=np.delete(self.pos,-1,axis=0)
        self.vel=np.delete(self.vel,-1,axis=0)
        self.mass=np.delete(self.mass,-1,axis=0)

    def reset(self,pos,vel):
        self.pos=pos
        self.vel=vel


class Trace():
    def __init__(self, wsp, siz, fig,ax,colour='w', icon="_"):
        self.loc = np.zeros((siz, len(wsp)))
        for i in range(siz):
            self.loc[i, :] = wsp

        self.colour = colour
        self.icon = icon  # symbol i kolor obiektu
        self.points, = ax.plot([], [], [], marker=icon, color=colour,
                               markersize=0.1, animated=True)

    def __repr__(self):
        return f"coordinates = {self.trace}"

    def update_trace(self, new_pos):
        self.loc[-1, :] = new_pos
        self.loc = np.roll(self.loc, 1, 0)
        return self.loc

    def update_image(self, trace_len):
        trace_points = np.zeros((trace_len,3))
        for i in range(trace_len):
            trace_points[i, :] = self.loc[i]

        trace_points = trace_points.T
        self.points.set_data_3d(
            trace_points[0], trace_points[1], trace_points[2])
